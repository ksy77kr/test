# Test

## 도커 빌드 하는방법 Example
```
docker build -t django_gunicorn:0.0.1 .
```

## 빌드한 도커 이미지를 실행하는 방법  Example
```
# localhost:8080으로 확인 하실 수 있습니다.
docker run -it -d --rm -p 8080:80 django_gunicorn:0.0.1
```

### 컨테이너 실행시 localhost:8080

<img width="1285" src="https://user-images.githubusercontent.com/16227780/59566313-62977580-9099-11e9-9151-9ee1f98da2de.png">